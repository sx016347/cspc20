command
what i think happens
what actually happened

cd ~
exits current directory
exists current directory

git init portfolio
creates a portfolio in git
creates a git repository in the directory 'portfolio'

cd portfolio
accesses the directory portfolio
accesses the directory portfolio

ls -al
unsure
shows the file name and the date it was last modified

git status
shows the state of the directory that is open at the time.
shows the state of the directory that is open at the time.

echo hello > .gitignore
stores hello into a file called gitignore
stores heloo into a file called gitignore

git add -A
adds a branch called A
stages all changes in the working directory at once

git status
displays the state of the current directory
displays the state of the cuurent directory

git config --global user.email"<student_email>"
Adds student email to user.email
sets git configuration levels on a global level

git config -global user.name "<student_email>"
returns an error
returns an error stating did i mean '--global'.

git commit -m "first commit, adding week 1 content"
creates a new commit with the message above with it.
creates a new commit with the message above with it.

git status
displays state of current directory in this case maybe would give information on the previous commit
displays state of current directory in this case says workig tree clean.

git push
no destination to push to.
no destination to push to e.g didnt include a URL for example.

git remote add origin https://csgitlab.reading.ac.uk/<student_id>/cs1pc20_portfolio.git
adds a location/repository for the next step to push its information to
sets a new remote repository as the origin for the local repository

git push –set-upstream origin master
pushes changes from one repository to another.
pushes changes from local repository to remote repository.

Git status
shows current state of the working directory.
shows current state of working directory and staging area.

echo “# CS1PC20 Portfolio” > readme.md
pushes “# CS1PC20 Portfolio” to the readme.md directory
pushes “# CS1PC20 Portfolio” to the readme.md directory.

Git add readme.md
Adds content of the file readme.md to the staging area.
Adds content of the file readme.md to the staging area.

git commit -m “added readme file”
saves changes.
saves changes to the local repository and displays the message added readme file. This message is displayed due to the -m in the command line.

Git push
Uploads content from local to remote repository
Uploads content from local to remote repository

git config –global credential.helper cache
joins one repository to another.
used to cache or save credentials for later use. For example, it will save our username and password so that you don’t have to enter them every time.

Git branch week2
Creates a new branch called week2
Creates a new branch called week2

Git checkout week2
Exits the current branch so in this case it exits week2.
Exits the current branch so in this case it exits week2.

Mkdir week2
Creates a directory called week2
Creates a directory called week2

echo “# Week 2 exercise observations” > week2/report.md 
error as there is no file called report.md
error as there is no file called report.md

git status
shows state of current working directory and staging area.
Shows state of current working directory and staging area.

Git add week2
Adds content of week2 to the staging area
Adds content of week2 to the staging area. The staging area store information about what will be in your next commit.

git commit -m “added week 2 folder and report.md” 
saves changes to the local repository
saves changes to the local repository and displays the message in quotation marks.

Git push
Uploads content from local to remote repository. 
Uploads content from local to remote repository. The remote repository is a version of the project hosted on the internet.

Git checkout master
Error as no such file as master
Switches to the master branch which is the default branch. This branch is created automatically.

Is -al
Lists all files and directories in the current directory. Also displays when directory was last accessed and modified.
Lists all files and directories in the current directory. Also displays when directory was last accessed and modified.

Git checkout week2
Exits current branch week2.
Exits current branch week2.

Ls -al
Displays directory and when it was last modified.
Displays directory and when it was last modified.

Git checkout master
Switches to the master branch which is the default branch. This branch is created automatically.
Switches to the master branch which is the default branch. This branch is created automatically.

Git merge week2
Joins one branch to another branch.
Joins one branch to another branch.

Ls -al
Shows the merged branches and the time they were last modified.
Shows the merged branches and the time they were last modified.

Git push
Uploads content from the local to the remote repository
Uploads content from the local to the remote repository.

rm -r week2
deletes week2 and all its contents.
Deletes week2 and all its contents.

Rm -r week1
Error a there is no such file as week1.
Error as there is no such file as week1.

Ls -al
Shows state of current directory and when it was last modified.
Shows state of current directory and when it was last modified.

Git status
shows state of current working directory and the staging area.
shows state of current working directory and the staging area.

Git stash
Stores data temporarily.
Takes uncommitted changes and saves them for later use and then reverts them from the working copy.

Git stash drop
Deletes stash
Deletes stash from the queue.

Ls -al
Shows state of current directory and when it was last modified.
Shows state of current directory and when it was last modified. Says a message about the deleted stash.

Cd ~
Exits current directory.
Exits current directory.

cp -r portfolio portfolio backup
makes a copy of the portfolio folder.
makes a copy of the portfolio folder.

rm -rf portfolio 
Deletes original portfolio folder.
Deletes original portfolio folder.

Ls -al
Shows state of current directory and when it was last modified.
Shows state of current directory and when it was last modified.

git clone https://csgitlab.reading.ac.uk/<student_id>/cs1pc20_portfolio portfolio
restores folder from csgitlab by cloning it
restores folder from csgitlab by cloning it.

